<?php

 function debug($data, $is_exit=false){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		if($is_exit){
			exit;
		}

	}

	function redirect($path, $key=null, $message=null ){
		if($key != null && $message !=null ){
			setFlash($key,$message);
		}
		header('location: '.$path);
		exit;
	}

	function setFlash($key,$message){
		if(!isset($_SESSION)){
			session_start();
		}
		$_SESSION[$key]=$message;
	}

	function flash(){
		if(isset($_SESSION['error']) && !empty($_SESSION['error'])){
			echo "<p class='alert alert-danger'>".$_SESSION['error']."</p>";
			unset($_SESSION['error']);
		}
		if(isset($_SESSION['success']) && !empty($_SESSION['success'])){
			echo "<p class='alert alert-success'>".$_SESSION['success']."</p>";
			unset($_SESSION['success']);
		}
	}


	function generateRandomString($length =100){
		$chars='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$strlen=strlen($chars);
		$random="";
	 	for ($i = 0; $i <$length ; $i++) {
			$pos=rand(0,$strlen-1);
			$random .= $chars[$pos];	 		
	 	}
	 	return $random;
	}


	function uploadSingleFile($file,$upload_dir){
		if($file['error']== 0){
			$ext =pathinfo($file['name'],PATHINFO_EXTENSION);
			if(in_array(strtolower($ext), ALLOWED_EXTENSION)){
				if($file['size']<=3000000){					
					$path = UPLOAD_DIR."/".$upload_dir;
					if(!is_dir($path)){
						mkdir($path, 0777, true);
					}
					$file_name= $upload_dir.'-'.date('Ymdhis').rand(0,999).".".$ext;

					$success=move_uploaded_file($file['tmp_name'],$path."/".$file_name);

					if($success){
						return $file_name;
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}