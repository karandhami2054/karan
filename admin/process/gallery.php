<?php
	require '../../config/init.php';
	require '../inc/checkLogin.php';

	$gallery = new Gallery();



/*	debug($_POST);
		debug($_FILES,true);*/
	if(isset($_POST) && !empty($_POST)){

		$data =array(
			'title'=>$_POST['title'],
			'summary'=>$_POST['summary'],
			'status'=>$_POST['status'],
			'added_by'=>$_SESSION['user_id']
		);	
		if(isset($_POST['del_image']) && file_exists(UPLOAD_DIR.'/gallery/'.$_POST['del_image'])){
			unlink(UPLOAD_DIR.'/gallery/'.$_POST['del_image']);
				$data['cover_image']='';
		}


		if(isset($_FILES, $_FILES['image']) && $_FILES['image']['error']==0){

			$file_name =uploadSingleFile($_FILES['image'],'gallery');
			if($file_name){
				$data['cover_image']= $file_name;
			}

		}
		$gallery_id= (isset($_POST['gallery_id']) && !empty($_POST['gallery_id'])) ? (int)$_POST['gallery_id'] : null;
		if($gallery_id){
			$act= "Updat";
			$gallery_id = $gallery->updateGallery($data,$gallery_id);
		}else{
			$act= "Add";
			$gallery_id = $gallery->addGallery($data);
		}

		if($gallery_id){
			redirect('../gallery.php','success','gallery '.$act.'ed successfully');
		}else{
			redirect('../gallery.php','error','failed to '.$act.' gallery.');
		}
		
	}elseif(isset($_GET) && !empty($_GET['id'])){
		/*debug($_GET,true);*/
		$gallery_id =(int)$_GET['id'];
		if($gallery_id<=0){
			redirect('../gallery.php','error','invalid gallery id.');
		}

		$gallery_info =$gallery->getGalleryById($gallery_id);
		if(!$gallery_info){
			redirect('../gallery.php','error','Gallery not found.');
		}

		$del =$gallery->deleteGallery($gallery_id);
		if($del){
			if(!empty($gallery_info[0]->image) && file_exists(UPLOAD_DIR.'/gallery/'.$gallery_info[0]->cover_image)){
				unlink(UPLOAD_DIR.'/gallery/'.$gallery_info[0]->cover_image);
		}
			redirect('../gallery.php','success','gallery deleted successfully.');
		}else{
			redirect('../gallery.php','error','gallery not deleted at this moment.');
		}
	}
	else{
		redirect('../gallery.php','error','unauthorized access.');
	}
