<?php 
require $_SERVER['DOCUMENT_ROOT'].'/broadwayNew/project/config/init.php';
$user = new User;
	if(isset($_POST) && !empty($_POST)){

		$user_name=filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);

		if(!$user_name){
		redirect('../','error','invalid user email');
		}

		$password = SHA1($user_name.$_POST['password']);
       /*DB validation*/
       	$user_info=$user->getUserByUsername($user_name);

		/*debug($user_info);*/
		if($user_info){
		if($user_info[0]->password==$password){
			if($user_info[0]->role == 'admin'){
				if($user_info[0]->status == 'active'){
					$_SESSION['user_id']= $user_info[0]->id;
					$_SESSION['full_name']= $user_info[0]->full_name;
					$_SESSION['email']= $user_info[0]->email;

					$token =generateRandomString(100);
					$_SESSION['token']= $token;

					if(isset($_POST,$_POST['remember']) && $_POST['remember']=='on'){

						setcookie('_au',$token,(time()+864000),'/');
						$user->updateRememberToken($token, $user_info[0]->id);

					}

					redirect('../dashboard.php','success' ,'Welcome to admin panel.');

				}else{
					redirect('../','error','You are no active');
				}
			}else{
			redirect('../','error','You are not admin');
			}
		}else{
			redirect('../','error','Password error');
		}
	}else{
			redirect('../','error','Username not found! invalid username.');	
	}

	}else{
		redirect('../','error','Unauthorized access.');
	}