<?php
if(!isset($_SESSION, $_SESSION['token']) || empty($_SESSION['token'])){

	if(isset($_COOKIE,$_COOKIE['_au']) && !empty($_COOKIE['_au'])){
		$token =$_COOKIE['_au'];

		$user = new User;
		$user_info = $user->getUserFromToken($token);
		if(!$user_info){
			setcookie('_au','',(time()-60),'/');
			redirect('./','error','please login first.');
		}

		$_SESSION['user_id'] =$user_info[0]->id;
		$_SESSION['full_name'] =$user_info[0]->full_name;
		$_SESSION['email']=$user_info[0]->email;
		$token = generateRandomString(100);
		$_SESSION['token'] =$token;

		setcookie('_au',$token, time()+864000, '/');

		$user->updateRememberToken($token, $user_info[0]->id);

	}else{
		redirect('./','error','please login first.');
	}
}

