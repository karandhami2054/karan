    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="./">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"><?php echo SITE_NAME ?>
        </div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="./">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="category.php">
          <i class="fas fa-fw fa-sitemap"></i>
          <span>Category Management</span>
          <i class="fas fa-fw fa-chevron-right"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="news.php">
          <i class="fas fa-fw fa-newspaper"></i>
          <span>News Management</span>
          <i class="fas fa-fw fa-chevron-right"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="gallery.php">
          <i class="fas fa-fw fa-image"></i>
          <span>Gallery Management</span>
          <i class="fas fa-fw fa-chevron-right"></i>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-video"></i>
          <span>Video Gallery Management</span>
          <i class="fas fa-fw fa-chevron-right"></i>
        </a>
      </li>
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>