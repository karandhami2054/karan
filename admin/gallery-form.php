<?php 
    require_once 'inc/header.php';
    require_once 'inc/checkLogin.php';
    $gallery =new Gallery;

    $act = "Add";
    if(isset($_GET, $_GET['id']) && !empty($_GET['id'])){
        $act= "Update";
        $gallery_id= (int)$_GET['id'];
        if($gallery_id<=0){
        redirect('gallery.php','error','Invalid gallery id ');
        }

        $gallery_info =$gallery->getGalleryById($gallery_id);

        if(!$gallery_info){
              redirect('gallery.php','error','gallery id not found.');
        }
         // debug($gallery_info,true);
    }
?>


  <!-- Page Wrapper -->
  <div id="wrapper">

     <?php require 'inc/sidebar.php'; ?>

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">


        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>

          </button>

         

         <?php require 'inc/top_nav.php'; ?>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800">Gallery <?php echo $act; ?>
            
          </h1>
          <?php flash(); ?>

          



          <div class="row">
            <div class="col-12">
              <form action="process/gallery.php" method="post" class="form" enctype="multipart/form-data">
                <div class="form-group row">
                  <label class="col-sm-3">Title:</label>
                  <div class="col-sm-9">
                    <input type="text" name="title" class="form-control form-control-sm" required="" value="<?php  echo @$gallery_info[0]->title; ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3">Summary:</label>
                  <div class="col-sm-9">
                    <textarea name="summary" id="summary"  rows="4" class="form-control form-control-sm" style="resize: none;" ><?php echo @$gallery_info[0]->summary; ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3">Status:</label>
                  <div class="col-sm-9">
                    <select name="status" id="status" class="form-control form-control-sm">
                      <option value="active"<?php echo (isset($gallery_info) && @$gallery_info[0]->status == 'active') ? 'selected' : ''  ?>>Active</option>
                      <option value="inactive" <?php echo (isset($gallery_info) && @$gallery_info[0]->status == 'inactive') ? 'selected' : '' ?>>Inactive</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3">Cover Image:</label>
                  <div class="col-sm-4">
                    <input type="file" name="image" accept="image/* ">
                    <div class="col-sm-4">
                        <?php   
                        if($act =='Update' && isset($gallery_info) && !empty($gallery_info[0]->cover_image) && file_exists(UPLOAD_DIR.'/gallery/'.$gallery_info[0]->cover_image)){
                            ?>
                                <img class="img img-thumbnail img-responsive" src="<?php echo UPLOAD_URL.'/gallery/'.$gallery_info[0]->cover_image  ?>" alt=""> 
                                <input type="checkbox" name="del_image" value="<?php echo $gallery_info[0]->cover_image ?>">delete

                        <?php
                        }
                         ?>    
                    </div>
                  </div>
                </div>

                 <div class="form-group row">
                  <label class="col-sm-3">Other image</label>
                  <div class="col-sm-9">
                    <input type="file" name="other_image[]" multiple="" accept="image/*">

                  </div>
                </div>



                <div class="form-group row">
                  <label class="col-sm-3"></label>
                  <div class="col-sm-9">
                    <input type="hidden" name="gallery_id" class="form-control form-control-sm" required="" value="<?php echo @$gallery_info[0]->id; ?>">
                  </div>
                </div>

                  <div class="form-group row">
                  <label class="col-sm-3"></label>
                  <div class="col-sm-9">
                   <button class="btn btn-danger" type="reset" >Reset</button>
                   <button class="btn btn-success" type="Submit" >Submit</button>

                  </div>
                </div>

              </form>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>



<?php require_once 'inc/footer.php'; ?>