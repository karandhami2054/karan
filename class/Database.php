<?php
  	
	abstract class Database{
		protected $conn =null;
		protected $sql=null;
		protected $stmt=null;
		protected $table=null;

		public function __construct(){
			try{
				$this->conn =new PDO('mysql:host='.DB_HOST.';dbname='.DN_NAME.';',DB_USER,DB_PWD);
				$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->sql= "SET NAMES utf8";
				$this->stmt =$this->conn->prepare($this->sql);
				$this->stmt->execute();


			}
			catch(PDOException $e){
				$message=date('Y-m-d H:i:s ')." Connection(PDO): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}
			catch(Exception $e){
				$message="Y-m-d h:i:s"." Connection(General): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}
		}

		final protected function table($_table){
			 $this->table =$_table;
		}

		final protected function select($args=array(), $query_debug =false){
			try{
				$this->sql ="SELECT ";


				if(isset($args,$args['fields']) && !empty($args['fields'])){
					if(is_array($args['fields'])){
						$this->sql .=implode(" , ", $args['fields']);
					}else{
						$this->sql .=$args['fields'];
					}
				}else{
					$this->sql .=" * ";	
				}


				$this->sql .=" FROM ";
				$this->sql .=$this->table;

				if(isset($args['where']) && !empty($args['where'])){
					if(is_string($args['where'])){
						$this->sql .=" where ".$args['where']; 
					}else{
						$temp =array();
						foreach($args['where'] as $column_name => $value ){
							$str =$column_name." = :".$column_name;
							$temp[]= $str;
						}
						$this->sql .=' where '.implode(' AND ',$temp);
					}
				}



				if(	isset($args['order_by']) && !empty($args['order_by'])){
					$this->sql .=" ORDER BY ".$args['order_by'];
				}else{
					$this->sql .=" ORDER BY ".$this->table.".id desc";
				}




				$this->stmt=$this->conn->prepare($this->sql);

				if(isset($args['where']) && !empty($args['where']) && is_array($args['where']) ){
					foreach($args['where'] as $column_name=>$value){

						if(is_int($value)){
							$param =PDO::PARAM_INT;
						}elseif(is_bool($value)){
							$param =PDO::PARAM_BOOL;
						}else{
							$param =PDO::PARAM_STR;
						}

						if($param){
							$this->stmt->bindvalue(":".$column_name,$value,$param);
						}
					}

				}
				/*debug($this->stmt);*/


				$this->stmt->execute();
                return $this->stmt->fetchAll(PDO::FETCH_OBJ);
					}


			catch(PDOException $e){
				$message=date('Y-m-d H:i:s ')." select(PDO): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}
			catch(Exception $e){
				$message="Y-m-d h:i:s"." select(General): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}

		}



		public function runRaw($sql){
			$this->stmt=$this->conn->prepare($sql);
			return $this->stmt->execute();
		}


		final protected function insert($data, $query_debug=false){
			try{

				$this->sql ="INSERT INTO ";
			
				if(!isset($this->table) || empty($this->table)){
					throw new Exception('table not found');
				}
				$this->sql .=$this->table;

				$this->sql .=" SET ";
				if(isset($data) && !empty($data)){
					if(is_string($data)){
						$this->sql .= $data; 
					}else{
						$temp =array();
						foreach($data as $column_name => $value ){
							$str =$column_name." = :".$column_name;
							$temp[]= $str;
						}
						$this->sql .= implode(' , ',$temp);
					}
				}

				if($query_debug){
					echo $this->sql;
					debug($data,true);
				}



				$this->stmt=$this->conn->prepare($this->sql);

				if(isset($data) && !empty($data) && is_array($data) ){
					foreach($data as $column_name=>$value){

						if(is_int($value)){
							$param =PDO::PARAM_INT;
						}elseif(is_bool($value)){
							$param =PDO::PARAM_BOOL;
						}else{
							$param =PDO::PARAM_STR;
						}

						if($param){
							$this->stmt->bindvalue(":".$column_name,$value,$param);
						}
					}

				}
				/*debug($this->stmt);*/


				$this->stmt->execute();
				return $this->conn->lastInsertId();
					}


			catch(PDOException $e){
				$message=date('Y-m-d H:i:s ')." INSERT(PDO): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}
			catch(Exception $e){
				$message="Y-m-d h:i:s"." INSERT(General): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}

		}


		final protected function update($data , $args=array(), $query_debug=false){
			try{

				$this->sql ="UPDATE ";
			
				if(!isset($this->table) || empty($this->table)){
					throw new Exception('table not found');
				}
				$this->sql .=$this->table;

				$this->sql .=" SET ";

				if(isset($data) && !empty($data)){
					if(is_string($data)){
						$this->sql .= $data; 
					}else{
						$temp =array();
						foreach($data as $column_name => $value ){
							$str =$column_name." = :".$column_name;
							$temp[]= $str;
						}
						$this->sql .= implode(' , ',$temp);
					}
				}


				if(isset($args['where']) && !empty($args['where'])){
					if(is_string($args['where'])){
						$this->sql .=" where ".$args['where']; 
					}else{
						$temp =array();
						foreach($args['where'] as $column_name => $value ){
							$str =$column_name." = :_".$column_name;
							$temp[]= $str;
						}
						$this->sql .=' where '.implode(' AND ',$temp);
					}
				}


				if($query_debug){
					echo $this->sql;
					debug($data,true);
				}



				$this->stmt=$this->conn->prepare($this->sql);

				if(isset($data) && !empty($data) && is_array($data) ){
					foreach($data as $column_name=>$value){

						if(is_int($value)){
							$param =PDO::PARAM_INT;
						}elseif(is_bool($value)){
							$param =PDO::PARAM_BOOL;
						}else{
							$param =PDO::PARAM_STR;
						}

						if($param){
							$this->stmt->bindvalue(":".$column_name,$value,$param);
						}
					}

				}

				if(isset($args['where']) && !empty($args['where']) && is_array($args['where']) ){
					foreach($args['where'] as $column_name=>$value){

						if(is_int($value)){
							$param =PDO::PARAM_INT;
						}elseif(is_bool($value)){
							$param =PDO::PARAM_BOOL;
						}else{
							$param =PDO::PARAM_STR;
						}

						if($param){
							$this->stmt->bindvalue(":_".$column_name,$value,$param);
						}
					}

				}
				/*debug($this->stmt);*/


				return $this->stmt->execute();
				debug($this->sql);
					}
					


			catch(PDOException $e){
				$message=date('Y-m-d H:i:s ')." UPDATE(PDO): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}
			catch(Exception $e){
				$message="Y-m-d h:i:s"." UPDATE(General): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}

		}






			final protected function delete($args=array(), $query_debug =false){
			try{
				$this->sql ="DELETE from ";

				$this->sql .=$this->table;

				if(isset($args['where']) && !empty($args['where'])){
					if(is_string($args['where'])){
						$this->sql .=" where ".$args['where']; 
					}else{
						$temp =array();
						foreach($args['where'] as $column_name => $value ){
							$str =$column_name." = :".$column_name;
							$temp[]= $str;
						}
						$this->sql .=' where '.implode(' AND ',$temp);
					}
				}


				$this->stmt=$this->conn->prepare($this->sql);

				if(isset($args['where']) && !empty($args['where']) && is_array($args['where']) ){
					foreach($args['where'] as $column_name=>$value){

						if(is_int($value)){
							$param =PDO::PARAM_INT;
						}elseif(is_bool($value)){
							$param =PDO::PARAM_BOOL;
						}else{
							$param =PDO::PARAM_STR;
						}

						if($param){
							$this->stmt->bindvalue(":".$column_name,$value,$param);
						}
					}

				}
				/*debug($this->stmt);*/


				return $this->stmt->execute();
               
					}


			catch(PDOException $e){
				$message=date('Y-m-d H:i:s ')." DELETE(PDO): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}
			catch(Exception $e){
				$message="Y-m-d h:i:s"." DELETE(General): ".$e->getMessage()."\r\n";
				error_log($message,3 ,ERROR_LOG);
			}

		}



	
	}
