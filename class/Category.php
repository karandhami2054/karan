<?php
 class Category extends Database{
 	public function __construct(){
 		parent::__construct();
 		$this->table('categories');
 	}

 	public function addCategory($data){
 		return $this->insert($data);
 	}

 	public function getAllCategories(){
 		return $this->select();
 	}

 	public function getCategoryById($cat_id){
 		$args=array(
 			'where'=>array(
 				'id'=>$cat_id
 			)
 		);
 		return $this->select($args);
 	}

 	public function deleteCategory($cat_id)
 	{
 		$args=array(
 			'where'=>array(
 				'id'=>$cat_id
 			)
 		);
 		return $this->delete($args);

 	}
 	public function updateCategory($data,$category_id){
 		$args= array(
 			'where'=>array(
 				'id'=> $category_id
 			)
 		);
 		$success= $this->update($data,$args);
 		if($success){
 			return $category_id;
 		}else{
 			return false;
 		}
 	}
 	
 }