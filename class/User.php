<?php
	class User extends Database{

		public function __construct(){
			parent::__construct();
			$this->table('users');
		}
		
		public function getUserByUsername($user_name){
			

			$condition=array(
				// 'fields'=>'id,full_name ,email, role ,password ,status',

				/*'fields'=>array('id', 'full_name','email','role','password','status'),*/
				
				// 'where'=> " email  ='".$user_name."' and status='active'   "
				'where'=>array( 
				   'email' =>$user_name
				   /*'status' =>'active'*/
				)
			); 
			return $this->select($condition);
		}

		public function updateRememberToken($token, $user_id){

			/*$sql="UPDATE users set remember_token ='".$token."' where id='".$user_id."'";
			return $this->runRaw($sql);*/
			$data =array(
				'remember_token'=>$token
			);
			$args =array(
				'where'=>array(
					'id'=> $user_id
				)
			);
			return $this->update($data,$args);

		}


		public function getUserFromToken($token){
			/*$sql= "SELECT * from users where remember_token'".$token."'   "*/
			$args =array(
				'where'=>array(
					'remember_token'=>$token
				)
			);
			return $this->select($args);
		}


		public function getUserByRole($reporter){
			$args =array(
				'where'=>array(
					'role'=>$reporter
				)
			);
			return $this->select($args);
		}
	}

