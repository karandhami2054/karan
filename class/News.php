<?php
 class News extends Database{
 	public function __construct(){
 		parent::__construct();
 		$this->table('news');
 	}

 	public function addNews($data){
 		return $this->insert($data);
 	}

 	public function getAllNews(){
 		return $this->select();
 	}

 	public function getNewsById($cat_id){
 		$args=array(
 			'where'=>array(
 				'id'=>$cat_id
 			)
 		);
 		return $this->select($args);
 	}

 	public function deleteNews($cat_id)
 	{
 		$args=array(
 			'where'=>array(
 				'id'=>$cat_id
 			)
 		);
 		return $this->delete($args);

 	}
 	public function updateNews($data,$news_id){
 		$args= array(
 			'where'=>array(
 				'id'=> $news_id
 			)
 		);
 		$success= $this->update($data,$args);
 		if($success){
 			return $news_id;
 		}else{
 			return false;
 		}
 	}
 	
 }