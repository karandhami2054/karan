<?php
 class Gallery extends Database{
 	public function __construct(){
 		parent::__construct();
 		$this->table('galleries');
 	}

 	public function addGallery($data){
 		return $this->insert($data);
 	}

 	public function getAllgalleries(){
 		return $this->select();
 	}

 	public function getGalleryById($gallery_id){
 		$args=array(
 			'where'=>array(
 				'id'=>$gallery_id
 			)
 		);
 		return $this->select($args);
 	}

 	public function deleteGallery($gallery_id)
 	{
 		$args=array(
 			'where'=>array(
 				'id'=>$gallery_id
 			)
 		);
 		return $this->delete($args);

 	}
 	public function updateGallery($data,$gallery_id){
 		$args= array(
 			'where'=>array(
 				'id'=> $gallery_id
 			)
 		);
 		$success= $this->update($data,$args);
 		if($success){
 			return $gallery_id;
 		}else{
 			return false;
 		}
 	}
 	
 }